//package pl.com.tt.InternetShop.repository;
//
//import org.springframework.data.repository.CrudRepository;
//import pl.com.tt.InternetShop.model.User;
//
//public interface UserRepository extends CrudRepository<User, Integer> {
//
//    User findByUsername(String username);
//
//    User findByEmail(String email);
//}