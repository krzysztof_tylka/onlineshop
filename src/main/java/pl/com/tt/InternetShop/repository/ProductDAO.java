package pl.com.tt.InternetShop.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.com.tt.InternetShop.model.CartProduct;
import pl.com.tt.InternetShop.model.Product;

import java.util.List;

@Transactional

@Repository
public class ProductDAO implements EntityDAO<Product> {


    @Autowired
    private SessionFactory sessionFactory;

//    Session session = sessionFactory.getCurrentSession(); dlaczego to nie działa ???

    @Override
    public List<Product> getEntitiesByString(String searchText) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Product where productName like '%" + searchText + "%'").getResultList();
    }

    @Override
    public List<Product> getAllEntities() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Product").getResultList();
    }

    public Product getEntityByCode(int productCode) {
        Session session = sessionFactory.getCurrentSession();
        return (Product) session.createQuery("from Product where productCode like '" + productCode + "'").getSingleResult();
    }

    public CartProduct getCartProductByCode(int productCode) {
        Product product = getEntityByCode(productCode);
        return new CartProduct(product.getProductCode(), product.getProductName(), product.getProductPrice(), product.getProductStock());
    }

    public void addToDatabase(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.save(product);
    }

    public void updateToDatabase(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.update(product);
    }

    public void clearDatabase() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete from Product");
        query.executeUpdate();
    }

    public void deleteProduct(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(product);
    }

    public void deleteOneProduct(Product product) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(product);
    }

    public Product getPriceByProductName(String text) {
        Session session = sessionFactory.getCurrentSession();
        return (Product) session.createQuery("from Product where productName like '%" + text + "%'").getSingleResult();
    }


    public void removeFromStock(int quantity) {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("");
    }
}
