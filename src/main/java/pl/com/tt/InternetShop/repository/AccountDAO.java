package pl.com.tt.InternetShop.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.com.tt.InternetShop.model.Account;
import pl.com.tt.InternetShop.model.Product;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Transactional
@Repository
public class AccountDAO {

    @Autowired
    private SessionFactory sessionFactory;

//    public Account findAccount(String userName) {
//        Session session = this.sessionFactory.getCurrentSession();
//        CriteriaBuilder builder = session.getCriteriaBuilder();
//        CriteriaQuery criteria = builder.createQuery();
//        Root<Account> i = criteria.from(Account.class);
//        criteria.select(i).where(
//                builder.equal(i.get("NAME"), userName)
//        );
//        Query<Account> query = session.createQuery(criteria);
//        return query.getSingleResult();
//    }

    public Account findAccount(String userName) {
        Session session = sessionFactory.getCurrentSession();
        return (Account) session.createQuery("from Account where userName like '" + userName + "'").getSingleResult();
    }

    public void addAccount(Account account) {
        Session session = sessionFactory.getCurrentSession();
        session.save(account);
    }

}