package pl.com.tt.InternetShop.repository;

import java.util.List;

public interface EntityDAO<T> {

    List<T> getEntitiesByString(String s);

    List<T> getAllEntities();

}
