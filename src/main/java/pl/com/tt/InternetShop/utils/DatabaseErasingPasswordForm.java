package pl.com.tt.InternetShop.utils;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.*;

public class DatabaseErasingPasswordForm {

    @NotEmpty(message = "Type any password")
    private  String password;

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
