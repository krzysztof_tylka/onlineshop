package pl.com.tt.InternetShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.tt.InternetShop.model.CartProduct;
import pl.com.tt.InternetShop.model.Product;
import pl.com.tt.InternetShop.model.ProductForm;
import pl.com.tt.InternetShop.repository.ProductDAO;

import java.io.*;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductDAO productDAO;

    public void add(ProductForm productForm) {
        productDAO.addToDatabase(new Product(productForm.getProductName(), productForm.getProductPrice(), productForm.getProductStock(), productForm.getProductDescription()));
    }

    public void update(ProductForm productForm, int productCode) {
        Product product = convertProductCodeToProduct(productCode);
        product.setProductName(productForm.getProductName());
        product.setProductPrice(productForm.getProductPrice());
        product.setProductStock(productForm.getProductStock());
        product.setProductDescription(productForm.getProductDescription());
        productDAO.updateToDatabase(product);
    }

    public void updateStock(int productCode, int quantity) {
        Product product = convertProductCodeToProduct(productCode);
        product.setProductStock(product.getProductStock() + quantity);
        productDAO.updateToDatabase(product);
    }

    public List<Product> findAllEntities() {
        return productDAO.getAllEntities();
    }

    public List<Product> findAllEntitiesByString(String text) {
        return productDAO.getEntitiesByString(text);
    }

    public void clearDatabase() {
        productDAO.clearDatabase();
    }

    public void deleteOneProduct(int productCode) {
        Product product = productDAO.getEntityByCode(productCode);
        productDAO.deleteProduct(product);
    }

    public Product convertProductCodeToProduct(int productCode) {
        return productDAO.getEntityByCode(productCode);
    }

    public CartProduct convertProductCodeToCartProduct(int productCode) {
        return productDAO.getCartProductByCode(productCode);
    }

    public void serialization() throws IOException {
        FileOutputStream f = new FileOutputStream(new File("databaseCopy.txt"));
        ObjectOutputStream o = new ObjectOutputStream(f);
        List<Product> products = productDAO.getAllEntities();
        for (Product product : products) {
            o.writeObject(product);
//            System.out.println(product.toString());
        }
        o.close();
        f.close();
    }

    public void deserialization() throws IOException, ClassNotFoundException {
        FileInputStream f = new FileInputStream(new File("databaseCopy.txt"));
        ObjectInputStream o = new ObjectInputStream(f);
        boolean endOfFileFlag = false;
        while (!endOfFileFlag) {
            try {
                Product product = (Product) o.readObject();
                productDAO.addToDatabase(product);
            } catch (EOFException e) {
                endOfFileFlag = true;
            }
        }
        o.close();
        f.close();
    }

    public void removeFromStock(int quantity) {
        productDAO.removeFromStock(quantity);
    }
}

