package pl.com.tt.InternetShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import pl.com.tt.InternetShop.model.CartProduct;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class CartService {

    private Map<Integer, CartProduct> productsInCart = new HashMap<>();

    private double total;

    @Autowired
    private ProductService productService;

    public Map<Integer, CartProduct> getProducts() {
        return productsInCart;
    }

    public double getTotal() {
        return total;
    }

    public String getTotalAsString() {
        DecimalFormat dec = new DecimalFormat("#0.00");
        return dec.format(total);
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void calculateTotal() {
        int total = (int) productsInCart
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .mapToDouble(v -> 100 * v.getProductPrice() * v.getQuantity())
                .sum();
        setTotal((double) total / 100);
        System.out.println("calculate:" + total);
    }

    public Integer getNumberOfItemsInCart() {
        return productsInCart.size();
    }

    public void addToCart(int productCode) {
        CartProduct product = productService.convertProductCodeToCartProduct(productCode);
        if (!productsInCart.containsKey(productCode)) {
            product.setQuantity(1);
            productsInCart.put(productCode, product);
            productService.updateStock(productCode, -1);
        }
    }

    /**
     * If product is in the map just increment quantity by 1.
     * If product is not in the map with, add it with quantity 1
     */

    public void plusProduct(int productCode) {
        CartProduct product = productService.convertProductCodeToCartProduct(productCode);
//        System.out.println(productsInCart.get(productCode).getQuantity());
//        System.out.println(product.getProductStock());

        if (productsInCart.get(productCode).getQuantity() < product.getProductStock()) {
            product.setQuantity(productsInCart.get(productCode).getQuantity() + 1);
            productService.updateStock(productCode, -1);
            productsInCart.put(productCode, product);
        }
    }

    /**
     * If product is in the map with quantity > 1, just decrement quantity by 1.
     * If product is in the map with quantity 1, remove it from map
     */

    public void minusProduct(int productCode) {
        CartProduct product = productService.convertProductCodeToCartProduct(productCode);
        if (productsInCart.get(productCode).getQuantity() > 1) {
            product.setQuantity(productsInCart.get(productCode).getQuantity() - 1);
            productService.updateStock(productCode, 1);
            productsInCart.put(productCode, product);
        }
    }

    public void deleteProductFromCart(int productCode) {
        CartProduct product = productService.convertProductCodeToCartProduct(productCode);
        int quantity = productsInCart.get(productCode).getQuantity();
        System.out.println("quantity on delete:"+ quantity);
        productService.updateStock(productCode, quantity);
        productsInCart.remove(productCode);
    }

    /**
     * @return unmodifiable copy of the map
     */

//    public Map<Product, Integer> getProductsInCart() {
//        return Collections.unmodifiableMap(products);
//    }

    /**
     * Checkout will rollback if there is not enough of some product in stock
     * @throws NotEnoughProductsInStockException
     */

//    public void checkout() throws NotEnoughProductsInStockException {
//        Product product;
//        for (Map.Entry<Product, Integer> entry : products.entrySet()) {
//            // Refresh quantity for every product before checking
//            product = productRepository.findOne(entry.getKey().getUserId());
//            if (product.getQuantity() < entry.getValue())
//                throw new NotEnoughProductsInStockException(product);
//            entry.getKey().setQuantity(product.getQuantity() - entry.getValue());
//        }
//        productRepository.save(products.keySet());
//        productRepository.flush();
//        products.clear();
//    }

//    public BigDecimal getTotal() {
//        return products.entrySet().stream()
//                .map(entry -> entry.getKey().getPrice().multiply(BigDecimal.valueOf(entry.getValue())))
//                .reduce(BigDecimal::add)
//                .orElse(BigDecimal.ZERO);
//    }

}
