package pl.com.tt.InternetShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.tt.InternetShop.model.Account;
import pl.com.tt.InternetShop.model.AccountForm;
import pl.com.tt.InternetShop.repository.AccountDAO;

@Service
public class AccountService {

    @Autowired
    private AccountDAO accountDAO;

    public Account findAccountByName(String accountName) {
        return accountDAO.findAccount(accountName);
    }

    public void addNewAccount(AccountForm accountForm) {
        Account account = new Account();
        account.setUserName(accountForm.getUserName());
        account.setUserEmail(accountForm.getUserEmail());
        account.setUserPassword(accountForm.getUserPassword());
        account.setUserRole(Account.CUSTOMER);
        account.setActive(true);
        accountDAO.addAccount(account);
        System.out.println(account.toString());

    }

}
