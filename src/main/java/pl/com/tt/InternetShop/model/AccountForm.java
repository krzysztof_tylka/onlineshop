package pl.com.tt.InternetShop.model;

import javax.persistence.Column;
import javax.validation.constraints.*;

public class AccountForm {

    @Size(min = 4, max = 10, message = "Name must have between 4 and 10 characters")
    private String userName;

    @NotEmpty(message = "Email field can't be empty!")
    @Email(message = "Input valid email address, please")
    private String userEmail;

    @Size(min = 3, max = 8, message = "Password must have between 3 and 8 characters")
    private String userPassword;

    @Size(min = 3, max = 30, message = "Full name must have between 3 and 30 characters")
    private String userFullname;

    @Size(max = 20, message = "Street must have max 20 characters")
    private String userStreet;

    @Size(max = 8, message = "Number must have max 8 characters")
    private String userStreetNumber;

    @Pattern(regexp = "\\d{2}-\\d{3}", message = "Postal Code field can't be empty!")
    private String userPostalCode;

    @NotEmpty(message = "City field can't be empty!")
    @Size(min = 2, max = 15, message = "City must have max 15 characters")
    private String userCity;

    @NotEmpty(message = "Phone number field can't be empty!")
    @Size(min = 9, max = 9, message = "Phone number must have 9 characters")
    private String userPhoneNumber;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserStreet() {
        return userStreet;
    }

    public void setUserStreet(String userStreet) {
        this.userStreet = userStreet;
    }

    public String getUserStreetNumber() {
        return userStreetNumber;
    }

    public void setUserStreetNumber(String userStreetNumber) {
        this.userStreetNumber = userStreetNumber;
    }

    public String getUserPostalCode() {
        return userPostalCode;
    }

    public void setUserPostalCode(String userPostalCode) {
        this.userPostalCode = userPostalCode;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    @Override
    public String toString() {
        return "AccountForm{" +
                "userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userFullname='" + userFullname + '\'' +
                ", userStreet='" + userStreet + '\'' +
                ", userStreetNumber=" + userStreetNumber +
                ", userPostalCode='" + userPostalCode + '\'' +
                ", userCity='" + userCity + '\'' +
                ", userPhoneNumber='" + userPhoneNumber + '\'' +
                '}';
    }
}
