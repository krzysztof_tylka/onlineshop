package pl.com.tt.InternetShop.model;

import java.io.Serializable;


public class CartProduct implements Serializable {

    private int productCode;
    private String productName;
    private double productPrice;
    private int productStock;
    private int quantity;

    public CartProduct(int productCode, String productName, double productPrice, int productStock) {

        this.productCode = productCode;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productStock = productStock;
    }

    public CartProduct() {
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductStock() {
        return productStock;
    }

    public void setProductStock(int productStock) {
        this.productStock = productStock;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "["
                + this.productCode
                + this.productName
                + this.productPrice
                + this.productStock
                + this.quantity
                + "]";
    }

}
