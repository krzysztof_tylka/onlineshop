//package pl.com.tt.InternetShop.model;
//
//import javax.validation.constraints.NotEmpty;
//import javax.validation.constraints.Positive;
//import javax.validation.constraints.Size;
//
//public class CustomerForm {
//
//    @NotEmpty(message = "Name field can not be empty!")
//    @Size(min = 4, max = 10, message = "Name has to be between 4 and 10 characters")
//    private String customerName;
//
//    @Size(min = 1, max = 30, message = "Street must have between 1 and 30 characters")
//    private String street;
//
//    @Size(min = 0, max = 5, message = "Number must have between 1 and 30 characters")
//    @Positive(message = "Number of street can't be negative!")
//    private int number;
//
//    @Size(min = 5, max = 5, message = "Postal Code must have 5 digits!")
//    private String postalCode;
//
//    @Size(min = 1, max = 20, message = "City must have between 1 and 20 characters")
//    private String city;
//
//    @Size(min = 3, max = 8, message = "Password must have between 3 and 8 characters")
//    private String customerPassword;
//
//    public String getUserPassword() {
//        return customerPassword;
//    }
//
//    public void setUserPassword(String customerPassword) {
//        this.customerPassword = customerPassword;
//    }
//
//    public String getStreet() {
//        return street;
//    }
//
//    public void setStreet(String street) {
//        this.street = street;
//    }
//
//    public int getNumber() {
//        return number;
//    }
//
//    public void setNumber(int number) {
//        this.number = number;
//    }
//
//    public String getPostalCode() {
//        return postalCode;
//    }
//
//    public void setPostalCode(String postalCode) {
//        this.postalCode = postalCode;
//    }
//
//    public String getCity() {
//        return city;
//    }
//
//    public void setCity(String city) {
//        this.city = city;
//    }
//
//    @Override
//    public String toString() {
//        return
//
//
//                null;
//    }
//}
