//package pl.com.tt.InternetShop.model;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import java.io.Serializable;
//
//@Entity
//@Table(name = "Customers")
//public class Customer implements Serializable {
//
//    @Id
//    @Column(name = "Id", length = 20, nullable = false)
//    private int customerId;
//
//    @Column(name = "Name", length = 20, nullable = false)
//    private String customerName;
//
//    @Column(name = "Street", length = 20, nullable = false)
//    private String customerStreet;
//
//    @Column(name = "Number", length = 20, nullable = false)
//    private int customerNumber;
//
//    @Column(name = "PostalCode", length = 20, nullable = false)
//    private String customerPostalCode;
//
//    @Column(name = "City", length = 20, nullable = false)
//    private String customerCity;
//
//    @Column(name = "Password", length = 20, nullable = false)
//    private String customerPassword;
//
//    public int getCustomerId() {
//        return customerId;
//    }
//
//    public void setCustomerId(int customerId) {
//        this.customerId = customerId;
//    }
//
//    public String getCustomerName() {
//        return customerName;
//    }
//
//    public void setCustomerName(String customerName) {
//        this.customerName = customerName;
//    }
//
//    public String getCustomerStreet() {
//        return customerStreet;
//    }
//
//    public void setCustomerStreet(String customerStreet) {
//        this.customerStreet = customerStreet;
//    }
//
//    public int getCustomerNumber() {
//        return customerNumber;
//    }
//
//    public void setCustomerNumber(int customerNumber) {
//        this.customerNumber = customerNumber;
//    }
//
//    public String getCustomerPostalCode() {
//        return customerPostalCode;
//    }
//
//    public void setCustomerPostalCode(String customerPostalCode) {
//        this.customerPostalCode = customerPostalCode;
//    }
//
//    public String getCustomerCity() {
//        return customerCity;
//    }
//
//    public void setCustomerCity(String customerCity) {
//        this.customerCity = customerCity;
//    }
//
//    public String getCustomerPassword() {
//        return customerPassword;
//    }
//
//    public void setCustomerPassword(String customerPassword) {
//        this.customerPassword = customerPassword;
//    }
//
//    @Override
//    public String toString() {
//        return "["
//                + this.customerId
//                + this.customerName
//                + this.customerStreet
//                + this.customerNumber
//                + this.customerCity
//                + this.customerPostalCode
//                + "]";
//    }
//}
//
