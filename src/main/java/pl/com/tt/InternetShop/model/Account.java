package pl.com.tt.InternetShop.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "accounts")
public class Account implements Serializable {

    public static final String MANAGER = "manager";
    public static final String EMPLOYEE = "employee";
    public static final String CUSTOMER = "customer";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", length = 4, nullable = false)
    private int userId;

    @Column(name = "Name", length = 10, nullable = false)
    private String userName;

    @Column(name = "Email", length = 30, nullable = false)
    private String userEmail;

    @Column(name = "Password", nullable = false)
    private String userPassword;

    @Column(name = "Role", length = 20, nullable = false)
    private String userRole = CUSTOMER;

    @Column(name = "Active", length = 1, nullable = false)
    private boolean active = true;

    @Column(name = "FullName", length = 30, nullable = true)
    private String userFullname;

    @Column(name = "Street", length = 20, nullable = true)
    private String userStreet;

    @Column(name = "Number", length = 10, nullable = true)
    private int userStreetNumber;

    @Column(name = "PostalCode", length = 5, nullable = true)
    private int userPostalCode;

    @Column(name = "City", length = 15, nullable = true)
    private String userCity;

    @Column(name = "Phone", length = 9, nullable = true)
    private int userPhoneNumber;

//    public Account(int userId, String userName, String userEmail, String userPassword, String userRole, boolean active, String userFullname, String userStreet, int userStreetNumber, String userPostalCode, String userCity) {
//        this.userId = userId;
//        this.userName = userName;
//        this.userEmail = userEmail;
//        this.userPassword = userPassword;
//        this.userRole = userRole;
//        this.active = active;
//        this.userFullname = userFullname;
//        this.userStreet = userStreet;
//        this.userStreetNumber = userStreetNumber;
//        this.userPostalCode = userPostalCode;
//        this.userCity = userCity;
//    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserStreet() {
        return userStreet;
    }

    public void setUserStreet(String userStreet) {
        this.userStreet = userStreet;
    }

    public int getUserStreetNumber() {
        return userStreetNumber;
    }

    public void setUserStreetNumber(int userStreetNumber) {
        this.userStreetNumber = userStreetNumber;
    }

    public int getUserPostalCode() {
        return userPostalCode;
    }

    public void setUserPostalCode(int userPostalCode) {
        this.userPostalCode = userPostalCode;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public int getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(int userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    @Override
    public String toString() {
        return "Account{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userRole='" + userRole + '\'' +
                ", active=" + active +
                ", userFullname='" + userFullname + '\'' +
                ", userStreet='" + userStreet + '\'' +
                ", userStreetNumber=" + userStreetNumber +
                ", userPostalCode='" + userPostalCode + '\'' +
                ", userCity='" + userCity + '\'' +
                ", userPhoneNumber='" + userPhoneNumber + '\'' +
                '}';
    }
}

