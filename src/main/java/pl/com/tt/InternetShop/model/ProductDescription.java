package pl.com.tt.InternetShop.model;

public class ProductDescription {

    private String productDescription;

    public ProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

}
