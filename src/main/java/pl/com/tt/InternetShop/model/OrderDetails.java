package pl.com.tt.InternetShop.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity

@Table(name = "orders_details")
public class OrderDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", length = 50, nullable = false)
    private String id;

    @Column(name = "Amount", nullable = false)
    private double orderAmount;

    @Column(name = "Price", nullable = false)
    private double orderPrice;

    @Column(name = "Quantity", length = 11, nullable = false)
    private int orderQuantity;

    @Column(name = "Order_Id", length = 50, nullable = false)
    private String orderId;

    @Column(name = "Product_Id", length = 20, nullable = false)
    private String productId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "["
                + this.id
                + this.orderAmount
                + this.orderPrice
                + this.orderQuantity
                + this.orderId
                + this.productId
                + "]";
    }
}

