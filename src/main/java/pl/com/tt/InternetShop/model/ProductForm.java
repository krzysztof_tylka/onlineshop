package pl.com.tt.InternetShop.model;

import javax.validation.constraints.*;

import static pl.com.tt.InternetShop.utils.Constants.DECRIPTION_MAX_LENGTH;

public class ProductForm {

    private int productCode;

//    @NotEmpty(message = "Name field can not be empty")
    @Size(min = 2, max = 30, message = "Name should contain between 2 and 30 characters")
    private String productName;

    @Min(value = 0, message = "Price minimum value must be greater or equals 0 PLN")
    @Max(value = 10000, message = "Price maximum value is set to 10000 PLN")
    private double productPrice;

    @Min(value = 0, message = "Stock minimum value must be greater or equals 0 pcs")
    @Max(value = 1000, message = "Stock maximum value is set to 1000 pcs")
    private int productStock;

    @Size(max = DECRIPTION_MAX_LENGTH, message = "Product description must have maximum 100 characters")
    private String productDescription;

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductStock() {
        return productStock;
    }

    public void setProductStock(int productStock) {
        this.productStock = productStock;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    @Override
    public String toString() {
        return "ProductForm: " +
                "(Name= " + productName +
                ", Price= " + productPrice +
                ", Stock= " + productStock +
                ", Description= " + productDescription +
                ')';
    }
}
