package pl.com.tt.InternetShop.model;

import javax.validation.constraints.Size;

public class SearchForm {

    @Size(min = 2, message = "Search phrase must have mininum 2 characters")
    @Size(max = 20, message = "Search phrase must have maximum 20 characters")
    private String searchText;

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getSearchText() {
        return searchText;
    }
}
