package pl.com.tt.InternetShop.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "products")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", length = 4, nullable = false)
    private int productCode;

    @Column(name = "Name", nullable = false)
    private String productName;

    @Column(name = "Price", nullable = false)
    private double productPrice;

    @Column(name = "Stock", length = 11, nullable = false)
    private int productStock;

    @Column(name = "Create_Date", nullable = false)
    private Date productDateOfCreation;

    @Column(name = "Description")
    private String productDescription;

    public Product(String productName, double productPrice, int productStock, String productDescription) {
        this.productName = productName;
        this.productPrice = productPrice;
        this.productStock = productStock;
        this.productDateOfCreation = new Date();
        this.productDescription = productDescription;
    }

    public Product() {
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductStock() {
        return productStock;
    }

    public void setProductStock(int productStock) {
        this.productStock = productStock;
    }

    public Date getProductDateOfCreation() {
        return productDateOfCreation;
    }

    public void setProductDateOfCreation(Date productDateOfCreation) {
        this.productDateOfCreation = productDateOfCreation;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    @Override
    public String toString() {
        return "["
                + this.productCode
                + this.productName
                + this.productPrice
                + this.productStock
                + this.productDateOfCreation
                + this.productDescription
                + "]";
    }

}
