//package pl.com.tt.InternetShop.model;
//
//import javax.persistence.*;
//import java.util.Objects;
//
//@Entity
//@Table(name = "users", schema = "spring.online.shop")
//public class User {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "id", unique = true)
//    private int userId;
//
//    @Column(name = "name")
//    private String userName;
//
//    @Column(name = "password")
//    private String userPassword;
//
//    @Transient
//    @Column(name = "password_2")
//    private String userPassword_2;
//
//    @Column(name = "email")
//    private String userEmail;
//
//    @Column(name = "role")
//    private int userRole;
//
//    public User() {
//    }
//
//    public User(String userName, String userPassword, String userEmail, int userRole) {
//        this.setUserName(userName);
//        this.setUserPassword(userPassword);
//        this.setUserEmail(userEmail);
//        this.setUserRole(userRole);
//    }
//
//    public int getUserId() {
//        return userId;
//    }
//
//    public void setUserId(int userId) {
//        this.userId = userId;
//    }
//
//    public String getUserName() {
//        return userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public String getUserPassword() {
//        return userPassword;
//    }
//
//    public void setUserPassword(String userPassword) {
//        this.userPassword = userPassword;
//    }
//
//    public String getUserPassword_2() {
//        return userPassword_2;
//    }
//
//    public void setUserPassword_2(String userPassword_2) {
//        this.userPassword_2 = userPassword_2;
//    }
//
//    public String getUserEmail() {
//        return userEmail;
//    }
//
//    public void setUserEmail(String userEmail) {
//        this.userEmail = userEmail;
//    }
//
//    public int getUserRole() {
//        return userRole;
//    }
//
//    public void setUserRole(int userRole) {
//        this.userRole = userRole;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        User user = (User) o;
//        return userId == user.userId &&
//                userRole == user.userRole &&
//                Objects.equals(userName, user.userName) &&
//                Objects.equals(userPassword, user.userPassword) &&
//                Objects.equals(userPassword_2, user.userPassword_2) &&
//                Objects.equals(userEmail, user.userEmail);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(userId, userName, userPassword, userPassword_2, userEmail, userRole);
//    }
//}
