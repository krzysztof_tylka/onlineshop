package pl.com.tt.InternetShop.model;

public class UserForm {

    private String fullname;
    private String street;
    private int streetNumber;
    private int postalCode;
    private String city;
    private int phoneNumber;

    public UserForm(String fullname, String street, int streetNumber, int postalCode, String city, int phoneNumber) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.phoneNumber = phoneNumber;
    }

    public UserForm() {
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Override
    public String toString() {
        return "UserForm {" +
                "fullname= " + fullname +
                ", address= " + street +" "+ streetNumber +", "+ postalCode +" "+ city +", "+
                ", phoneNumber= " + phoneNumber +
                '}';
    }
}
