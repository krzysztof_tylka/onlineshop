package pl.com.tt.InternetShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.com.tt.InternetShop.model.Product;
import pl.com.tt.InternetShop.model.ProductForm;
import pl.com.tt.InternetShop.model.SearchForm;
import pl.com.tt.InternetShop.service.CartService;
import pl.com.tt.InternetShop.service.ProductService;
import pl.com.tt.InternetShop.utils.DatabaseErasingPassword;
import pl.com.tt.InternetShop.utils.DatabaseErasingPasswordForm;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ProductController {

    private ProductForm currentProductForm;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartService cartService;

    @RequestMapping(value = "/products/admin", method = RequestMethod.GET)
    public String productsAdminMenu(
            Model model) throws Exception {
        List<Product> products = productService.findAllEntities();
        if (products == null || products.size() == 0) {
            model.addAttribute("content", "nothing");
            model.addAttribute("message", "databaseEmpty");
            model.addAttribute("searchForm", new SearchForm());
            model.addAttribute("itemsInCart", cartService.getProducts().size());
            model.addAttribute("total", cartService.getTotalAsString());
            return "messageLayout";
        }
        model.addAttribute("content", "productsAdminView");
        model.addAttribute("products", products);
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
            model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping(value = "/products/add", method = RequestMethod.GET)
    public String productAdd(
            Model model) throws Exception {
        model.addAttribute("content", "productAdd");
        model.addAttribute("product", new ProductForm());
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping(value = "/product/info/{productCode}", method = RequestMethod.GET)
    public String productInfo(
            Model model,
            @PathVariable("productCode") int productCode) throws Exception {
        Product product = productService.convertProductCodeToProduct(productCode);
        model.addAttribute("content", "productInfo");
        model.addAttribute("product", product);
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }


    @RequestMapping(value = "/products/add", method = RequestMethod.POST)
    public String productAddFormDataCheck(
            Model model,
            @Valid @ModelAttribute("product") ProductForm productForm,
            BindingResult productBindingResult,
            @Valid @ModelAttribute("searchForm") SearchForm searchForm,
            BindingResult searchBindingResult) throws Exception {

        if (productBindingResult.hasErrors()) {
            model.addAttribute("content", "productAdd");
            model.addAttribute("product", productForm);

            model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
            return "index";
        }
        if (searchBindingResult.hasErrors()) {
            List<Product> products = productService.findAllEntities();
            model.addAttribute("content", "productsAdminView");
            model.addAttribute("products", products);
            model.addAttribute("searchForm", searchForm);

            model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
            return "index";
        }
        model.addAttribute("content", "productAddFormDataCheck");
        model.addAttribute("product", productForm);
        currentProductForm = productForm;
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping(value = "/products/added", method = RequestMethod.GET)
    public String addedProductConfirmation(
            Model model) throws Exception {
        productService.add(currentProductForm);
        List<Product> products = productService.findAllEntities();
        model.addAttribute("content", "productsAdminView");
        model.addAttribute("message", "productAdded");
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("products", products);
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "messageLayout";
    }

    @RequestMapping(value = "/products/modifyNewProductData", method = RequestMethod.GET)
    public String modifyDataOfNewProduct(
            Model model) throws Exception {
        model.addAttribute("content", "productAdd");
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("product", currentProductForm);
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping(value = "products/edit/{productCode}", method = RequestMethod.GET)
    public String productEditGetRequestMethod(
            Model model,
            @PathVariable int productCode) throws Exception {
        ProductForm productForm = new ProductForm();
        SearchForm searchForm = new SearchForm();
        model.addAttribute("content", "productEdit");
        Product editedProduct = productService.convertProductCodeToProduct(productCode);
//        productForm.setProductCode(editedProduct.getProductCode());
        productForm.setProductName(editedProduct.getProductName());
        productForm.setProductPrice(editedProduct.getProductPrice());
        productForm.setProductStock(editedProduct.getProductStock());
        productForm.setProductDescription(editedProduct.getProductDescription());
        model.addAttribute("product", productForm);
        model.addAttribute("productCode", productCode);
        model.addAttribute("searchForm", searchForm);
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping(value = "products/edit/{productCode}", method = RequestMethod.POST)
    public String productEditPostRequestMethod(
            Model model,
            @Valid @ModelAttribute("product") ProductForm productForm,
            BindingResult productBindingResult,
            @Valid @ModelAttribute("searchForm") SearchForm searchForm,
            BindingResult searchBindingResult,
            @PathVariable("productCode") int productCode) throws Exception {
        if (productBindingResult.hasErrors()) {
            model.addAttribute("content", "productEdit");
            model.addAttribute("product", productForm);
            model.addAttribute("searchForm", new SearchForm());

            model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
            return "index";
        }
        if (searchBindingResult.hasErrors()) {
            List<Product> products = productService.findAllEntities();
            model.addAttribute("content", "productsAdminView");
            model.addAttribute("products", products);
            model.addAttribute("searchForm", searchForm);

            model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
            return "index";
        }
        model.addAttribute("content", "productEditFormConfirmation");
        model.addAttribute("product", productForm);
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("productCode", productCode);
        currentProductForm = productForm;
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping(value = "/products/modifyEditedProductData/{productCode}", method = RequestMethod.GET)
    public String modifyEditedProductData(
            Model model,
            @PathVariable("productCode") int productCode) throws Exception {
        model.addAttribute("content", "productEdit");
        model.addAttribute("productCode", productCode);
        model.addAttribute("product", currentProductForm);
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping(value = "/products/edited/{productCode}", method = RequestMethod.GET)
    public String editedProductConfirmation(
            Model model,
            @PathVariable("productCode") int productCode) throws Exception {
        model.addAttribute("content", "productsAdminView");
        model.addAttribute("message", "productEdited");
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("productCode", productCode);
        currentProductForm.setProductCode(productCode);
        productService.update(currentProductForm, productCode);
        List<Product> products = productService.findAllEntities();
        model.addAttribute("products", products);
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "messageLayout";
    }

    @RequestMapping(value = "products/delete/{productCode}", method = RequestMethod.GET)
    public String deleteSingleProduct(
            Model model,
            @PathVariable int productCode) throws Exception {
        Product product = productService.convertProductCodeToProduct(productCode);
        model.addAttribute("content", "productDeleteConfirmation");
        model.addAttribute("product", product);
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";

    }

    @RequestMapping(value = "/products/deleted/{productCode}", method = RequestMethod.GET)
    public String deletedProductConfirmation(
            Model model,
            @PathVariable("productCode") int productCode) throws Exception {
        productService.deleteOneProduct(productCode);
        List<Product> products = productService.findAllEntities();
        model.addAttribute("content", "productsAdminView");
        model.addAttribute("message", "productDeleted");
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("productCode", productCode);
        model.addAttribute("products", products);
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "messageLayout";
    }

    @RequestMapping(value = "products/databaseErasingPassword", method = RequestMethod.GET)
    public String showDatabaseErasingPassword(
            Model model) throws Exception {
        model.addAttribute("content", "databaseErasingPassword");
        model.addAttribute("passwordForm", new DatabaseErasingPasswordForm());
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping(value = "products/databaseErasingPassword", method = RequestMethod.POST)
    public String verifydatabaseErasingPassword(
            Model model,
            @ModelAttribute("passwordForm") DatabaseErasingPasswordForm databaseErasingPasswordForm) throws Exception {
        if (!DatabaseErasingPassword.getPASSWORD().equals(databaseErasingPasswordForm.getPassword())) {
            model.addAttribute("content", "databaseErasingPassword");
            model.addAttribute("passwordForm", new DatabaseErasingPasswordForm());
            model.addAttribute("searchForm", new SearchForm());

            model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
            return "index";
        }
        model.addAttribute("content", "databaseErasingConfirmation");
        productService.serialization();
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping(value = "products/databaseCleared", method = RequestMethod.GET)
    public String afterErasingDatabaseMessage(
            Model model) throws Exception {
        model.addAttribute("content", "productsAdminView");
        model.addAttribute("message", "databaseEmpty");
        model.addAttribute("searchForm", new SearchForm());
        productService.clearDatabase();
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "messageLayout";
    }

}
