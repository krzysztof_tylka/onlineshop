package pl.com.tt.InternetShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.com.tt.InternetShop.model.SearchForm;
import pl.com.tt.InternetShop.service.CartService;
import pl.com.tt.InternetShop.service.ProductService;

@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    @Autowired
    private ProductService productService;

    @RequestMapping("/cart/addProduct/{productCode}")
    public String cart(
            Model model,
            @PathVariable("productCode") int productCode) throws Exception {
        cartService.addToCart(productCode);
        model.addAttribute("content", "productsAdminView");
        model.addAttribute("products", productService.findAllEntities());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        cartService.calculateTotal();
        model.addAttribute("total", cartService.getTotalAsString());
        model.addAttribute("searchForm", new SearchForm());
        return "index";
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public String addingProductToCart(
            Model model) throws Exception {
        model.addAttribute("content", "cart");
        model.addAttribute("items", cartService.getProducts());
        cartService.calculateTotal();
        model.addAttribute("total_int", cartService.getTotal());
        model.addAttribute("total", cartService.getTotalAsString());
        return "cartLayout";
    }

    @RequestMapping(value = "/cart/productUp/{productCode}", method = RequestMethod.GET)
    public String plusQuantityInCart(
            Model model,
            @PathVariable("productCode") int productCode) throws Exception {
        cartService.plusProduct(productCode);
        model.addAttribute("content", "cart");
        model.addAttribute("items", cartService.getProducts());
        cartService.calculateTotal();
        model.addAttribute("total_int", cartService.getTotal());
        model.addAttribute("total", cartService.getTotalAsString());
        return "cartLayout";
    }

    @RequestMapping(value = "/cart/productDown/{productCode}", method = RequestMethod.GET)
    public String minusQuantityInCart(
            Model model,
            @PathVariable("productCode") int productCode) throws Exception {
        cartService.minusProduct(productCode);
        model.addAttribute("content", "cart");
        model.addAttribute("items", cartService.getProducts());
        cartService.calculateTotal();
        model.addAttribute("total_int", cartService.getTotal());
        model.addAttribute("total", cartService.getTotalAsString());
        return "cartLayout";
    }

    @RequestMapping(value = "/cart/productRemove/{productCode}", method = RequestMethod.GET)
    public String removeProductFromCart(
            Model model,
            @PathVariable("productCode") int productCode) throws Exception {
        cartService.deleteProductFromCart(productCode);
        model.addAttribute("content", "cart");
        model.addAttribute("items", cartService.getProducts());
        cartService.calculateTotal();
        model.addAttribute("total_int", cartService.getTotal());
        model.addAttribute("total", cartService.getTotalAsString());
        return "cartLayout";
    }

    @RequestMapping(value = "/cart/checkout", method = RequestMethod.GET)
    public String checkout(
            Model model) throws Exception {
        model.addAttribute("content", "cartCheckout");
        model.addAttribute("items", cartService.getProducts());
        cartService.calculateTotal();
        model.addAttribute("total_int", cartService.getTotal());
        model.addAttribute("total", cartService.getTotalAsString());
        return "cartLayout";
    }


}