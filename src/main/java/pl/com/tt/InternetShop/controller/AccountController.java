package pl.com.tt.InternetShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.com.tt.InternetShop.model.Account;
import pl.com.tt.InternetShop.model.AccountForm;
import pl.com.tt.InternetShop.service.AccountService;
import pl.com.tt.InternetShop.service.CartService;

import javax.validation.Valid;

@Controller
public class AccountController {

    private AccountForm currentAccountForm;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CartService cartService;


    @RequestMapping(value = "/account/add", method = RequestMethod.GET)
    public String accountAddingGetMethod(
            Model model) throws Exception {
        model.addAttribute("content", "accountAdd");
        model.addAttribute("account", new AccountForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "loginLayout";
    }

    @RequestMapping(value = "/account/add", method = RequestMethod.POST)
    public String accountAddingPostMethod(
            Model model,
            @Valid @ModelAttribute("account") AccountForm accountForm,
            BindingResult result) throws Exception {
        System.out.println(result.hasErrors());
        if (result.hasErrors()) {
            model.addAttribute("content", "accountAdd");
            model.addAttribute("account", accountForm);
            model.addAttribute("itemsInCart", cartService.getProducts().size());
            model.addAttribute("total", cartService.getTotalAsString());
//            System.out.println("result has errors");
            return "loginLayout";
        }
        model.addAttribute("content", "accountAddFormDataChecking");
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        model.addAttribute("account", accountForm);
//        currentAccountForm = accountForm;
//        System.out.println("test ok");
        return "loginLayout";
    }

    @RequestMapping(value = "/account/modifyNewAccountData", method = RequestMethod.GET)
    public String checkingDataOfNewAccount(
            Model model) throws Exception {
        model.addAttribute("content", "accountAdd");
        model.addAttribute("account", currentAccountForm);
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "loginLayout";
    }

    @RequestMapping(value = "/account/signin", method = RequestMethod.GET)
    public String loginGet(Model model) throws Exception {
        model.addAttribute("content", "signin");
        return "loginLayout";
    }

    @RequestMapping(value = "/account/signin", method = RequestMethod.POST)
    public String loginPost(Model model) throws Exception {
        model.addAttribute("content", "signin");
        return "loginLayout";
    }

//    @RequestMapping(value = "/account/loginError")
//    public String loginError(
//            Model model) throws Exception {
//        model.addAttribute("loginError", true);
//        model.addAttribute("content", "loginError");
//
//        System.out.println("formName:" + accountForm.getUserName());
//        System.out.println("formPassword:" + accountForm.getUserPassword());
//        Account account = accountService.findAccountByName(accountForm.getUserName());
//        if (account == null) {
//            model.addAttribute("content", "accountLogin");
//            model.addAttribute("message", "noAccount");
//            model.addAttribute("account", accountForm);
//            return "loginMessageLayout";
//        }
//        System.out.println("test DAO accountName:" + account.getUserName());
//        System.out.println("test DAO accountPAssword:" + account.getUserPassword());
//        System.out.println("test DAO accountRole:" + account.getUserRole());
//        model.addAttribute("content", "accountInfo");
//        return "loginLayout";
//    }
//
//    @RequestMapping(value = "/account/loginError", method = RequestMethod.GET)
//    public String accountLoginError(
//            Model model) throws Exception {
//        model.addAttribute("content", "contact");
//        model.addAttribute("account", new AccountForm());
//        return "loginLayout";
//    }

    @RequestMapping(value = "/account/address/add/{userName}", method = RequestMethod.GET)
    public String accountAddingAddressGetMethod(
            Model model,
            @PathVariable("userName") String userName) throws Exception {
        model.addAttribute("content", "shippingDataForm");
        model.addAttribute("account", accountService.findAccountByName(userName));
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "loginLayout";
    }

    @RequestMapping(value = "/account/address/add/{userName}", method = RequestMethod.POST)
    public String accountAddingAddressPostMethod(
            Model model,
            @ModelAttribute("account") AccountForm accountForm) throws Exception {
        model.addAttribute("content", "shippingDataForm");
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        currentAccountForm = accountForm;
        model.addAttribute("account", currentAccountForm);
        return "loginLayout";
    }

    @RequestMapping(value = "/account/changePassword/{userName}", method = RequestMethod.GET)
    public String accountChangePasswordGetMethod(
            Model model,
            @PathVariable("userName") String userName) throws Exception {
        Account account = accountService.findAccountByName(userName);
        model.addAttribute("content", "passwordChange");
        model.addAttribute("account", account);
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "loginLayout";
    }

    @RequestMapping(value = "/account/info", method = RequestMethod.GET)
    public String accountInfo(
            Model model) throws Exception {
        accountService.addNewAccount(currentAccountForm);
        Account account = accountService.findAccountByName(currentAccountForm.getUserName());
        System.out.println("info: " + account.toString());
        model.addAttribute("account", account);
        model.addAttribute("content", "accountInfo");
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "loginLayout";
    }

    @RequestMapping(value = "/account/edit/{accountName}", method = RequestMethod.GET)
    public String accounEdit(
            Model model,
            @PathVariable("accountName") String accountName) throws Exception {
//        System.out.println("accountInfo:" + accountName);
        model.addAttribute("content", "nothing");
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "loginLayout";
    }

    @RequestMapping(value = "/account/changePassword/{accountName}", method = RequestMethod.GET)
    public String accountChangePassword(
            Model model,
            @PathVariable("accountName") String accountName) throws Exception {
//        System.out.println("accountInfo:" + accountName);
        model.addAttribute("content", "nothing");
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "loginLayout";
    }

    @RequestMapping(value = "/admin/product", method = RequestMethod.GET)
    public String adminProduct(
            Model model) throws Exception {
        model.addAttribute("content", "productsAdminView");
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
//        model.addAttribute("account", new AccountForm());
        return "loginLayout";
    }


}
