package pl.com.tt.InternetShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.com.tt.InternetShop.model.AccountForm;
import pl.com.tt.InternetShop.model.Product;
import pl.com.tt.InternetShop.model.SearchForm;
import pl.com.tt.InternetShop.service.CartService;
import pl.com.tt.InternetShop.service.ProductService;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@Controller
public class MainController {

    @Autowired
    private CartService cartService;

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String root(Locale locale,
                       ModelMap model) throws Exception {
        List<Product> products = productService.findAllEntities();
        System.out.println(products);
        if (products == null || products.size() == 0) {
            model.addAttribute("content", "productsAdminView");
            model.addAttribute("message", "noProductMessage");
            model.addAttribute("searchForm", new SearchForm());
            model.addAttribute("itemsInCart", cartService.getProducts().size());
            model.addAttribute("total", cartService.getTotalAsString());
            return "messageLayout";
        }
        model.addAttribute("content", "productsAdminView");
        model.addAttribute("products", products);
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        model.addAttribute("searchForm", new SearchForm());
        return "index";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String productSearching(
            Model model,
            @Valid @ModelAttribute("searchForm") SearchForm searchForm,
            BindingResult result) throws Exception {
        String searchText = searchForm.getSearchText();
        if (result.hasErrors()) {
            model.addAttribute("content", "productsAdminView");
            model.addAttribute("products", productService.findAllEntities());
            model.addAttribute("itemsInCart", cartService.getProducts().size());
            model.addAttribute("total", cartService.getTotalAsString());
            model.addAttribute("searchForm", searchForm);
            return "index";
        }
        List<Product> searchedProducts = productService.findAllEntitiesByString(searchText);
        if (searchedProducts == null || searchedProducts.size() == 0) {
            model.addAttribute("products", productService.findAllEntities());
            model.addAttribute("content", "productsAdminView");
            model.addAttribute("message", "searchingEmpty");
            model.addAttribute("searchForm", new SearchForm());
            model.addAttribute("itemsInCart", cartService.getProducts().size());
            model.addAttribute("total", cartService.getTotalAsString());
            return "messageLayout";
        }
        model.addAttribute("content", "productsSearchingResults");
        model.addAttribute("products", searchedProducts);
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping("/contact")
    public String contact(
            Model model) throws Exception {
        model.addAttribute("content", "contact");
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

    @RequestMapping("/info")
    public String info(
            Model model) throws Exception {
        model.addAttribute("content", "info");
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("itemsInCart", cartService.getProducts().size());
        model.addAttribute("total", cartService.getTotalAsString());
        return "index";
    }

//    @RequestMapping(value = "/restore", method = RequestMethod.GET)
//    public String restoreDatabase(
//            Model model) throws Exception {
//        model.addAttribute("content", "productsAdminView");
//        List<Product> products = productService.findAllEntities();
//        if (products.size() == 0 || products == null) {
//            productService.deserialization();
//        }
//        List<Product> restoredProducts = productService.findAllEntities();
//        model.addAttribute("products", restoredProducts);
//        model.addAttribute("searchForm", new SearchForm());
//        model.addAttribute("itemsInCart", cartService.getProducts().size());
//        model.addAttribute("total",cartService.getTotalAsString());
//        return "index";
//    }


}