package pl.com.tt.InternetShop.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.com.tt.InternetShop.service.AuthUserDetailsService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthUserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

// Setting Service to find User in the database and Setting PassswordEncoder
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

// Requires login with role EMPLOYEE or MANAGER. If not, it will redirect to /login.
        http.authorizeRequests().antMatchers("/products/**", "/contact/**", "/info/**", "/login/**", "/signup/**", "/cart/**").permitAll();
        http.authorizeRequests().antMatchers("/admin/**").access("hasAnyRole('EMPLOYEE', 'MANAGER')");
//        ,"/admin/orderList", "/admin/order", "/admin/userInfo"

// Pages only for MANAGER
        http.authorizeRequests().antMatchers("/admin/product/**", "admin/products/**", "/restore").access("hasRole('MANAGER')");

        // When user login with role XX, but access to the page requires the YY role, an AccessDeniedException will be thrown.
        http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");

        // Configuration for Login Form.
        http.authorizeRequests().and().formLogin()//
                .loginPage("/login")//
                .loginProcessingUrl("/j_spring_security_check") // Submit URL
                .defaultSuccessUrl("/account/info")//
                .failureUrl("/loginError")//
                .usernameParameter("userName")//
                .passwordParameter("userPassword")

                // Configuration for the Logout page. (After logout, go to home page)
                .and().logout().logoutUrl("/logout").logoutSuccessUrl("/");
    }
}